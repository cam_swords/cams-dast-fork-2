#!/bin/sh

set -e

printf "\n\n######### Initializing environment variables #########\n"

if [ -z "$GITLAB_API_TOKEN" ]; then
  printf "Aborting, environment variable GITLAB_API_TOKEN must contain a GitLab private token with access to this project.\n"
  exit 1
fi

# required file paths
WORKING_DIRECTORY="$(dirname "$(realpath "$0")")"
FILE_CONTAINING_IMAGE_NAME=$(realpath "$WORKING_DIRECTORY/../built_image.txt")
FILE_CONTAINING_SUPPORTED_GITLAB_VERSION=$(realpath "$WORKING_DIRECTORY/../supported-gitlab-runner.txt")

if ! [ -f "$FILE_CONTAINING_IMAGE_NAME" ]; then
  printf "Aborting, unable to determine previously built/tested DAST image as file containing name doesn't exist: '%s'.\n" "$FILE_CONTAINING_IMAGE_NAME"
  exit 1
fi

if ! [ -f "$FILE_CONTAINING_SUPPORTED_GITLAB_VERSION" ]; then
  printf "Aborting, unable to determine supported GitLab version as the file containing the version doesn't exist: '%s'.\n" "$FILE_CONTAINING_SUPPORTED_GITLAB_VERSION"
  exit 1
fi

RELEASE_DATA=$("$WORKING_DIRECTORY/parse-changelog.sh")
DAST_IMAGE=$(cat "$FILE_CONTAINING_IMAGE_NAME")
GITLAB_RUNNER_VERSION=$(cat "$FILE_CONTAINING_SUPPORTED_GITLAB_VERSION")
VERSION=$(echo "$RELEASE_DATA" | jq --raw-output .tag_name)
MAJOR_MINOR=$(echo "$VERSION" | awk -F '.' '{print $1"."$2}')
MAJOR=$(echo "$VERSION" | awk -F '.' '{print $1}')

printf "\n\n######### Determined release version of DAST to be %s, compatible with GitLab %s #########\n" "$VERSION" "$GITLAB_RUNNER_VERSION"
printf "\n\n######### Verifying DAST %s is not already present #########\n" "$VERSION"

# verify a release with this version does not already exist
if curl --silent --fail --show-error --header "private-token:$GITLAB_API_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/repository/tags/$VERSION"; then
  printf "\nAborting, tag %s already exists. If this is not expected, please remove the tag and try again.\n" "$VERSION"
  exit 1
fi

printf "\n\n######### Authenticating with Docker #########\n"
docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" "$CI_REGISTRY"

printf "\n\n######### Retrieving DAST Docker image %s so it can be tagged #########\n" "$DAST_IMAGE"
docker pull "$DAST_IMAGE"

printf "\n\n######### Creating Docker tag %s:%s #########\n" "$CI_REGISTRY_IMAGE" "$VERSION"
docker tag "$DAST_IMAGE" "$CI_REGISTRY_IMAGE:$VERSION"
docker push "$CI_REGISTRY_IMAGE:$VERSION"

printf "\n\n######### Creating Docker tag %s:%s #########\n" "$CI_REGISTRY_IMAGE" "$MAJOR_MINOR"
docker tag "$DAST_IMAGE" "$CI_REGISTRY_IMAGE:$MAJOR_MINOR"
docker push "$CI_REGISTRY_IMAGE:$MAJOR_MINOR"

printf "\n\n######### Creating Docker tag %s:%s #########\n" "$CI_REGISTRY_IMAGE" "$MAJOR"
docker tag "$DAST_IMAGE" "$CI_REGISTRY_IMAGE:$MAJOR"
docker push "$CI_REGISTRY_IMAGE:$MAJOR"

printf "\n\n######### Creating Docker tag %s:latest #########\n" "$CI_REGISTRY_IMAGE"
docker tag "$DAST_IMAGE" "$CI_REGISTRY_IMAGE:latest"
docker push "$CI_REGISTRY_IMAGE:latest"

printf "\n\n######### Creating Docker tag %s:%s #########\n" "$CI_REGISTRY_IMAGE" "$GITLAB_RUNNER_VERSION"
docker tag "$DAST_IMAGE" "$CI_REGISTRY_IMAGE:$GITLAB_RUNNER_VERSION"
docker push "$CI_REGISTRY_IMAGE:$GITLAB_RUNNER_VERSION"

printf "\n\n######### Tagging Git SHA %s with %s #########\n" "$CI_COMMIT_SHA" "$VERSION"
curl --silent --fail --show-error --request POST --header "PRIVATE-TOKEN:$GITLAB_API_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/repository/tags?tag_name=$VERSION&ref=$CI_COMMIT_SHA"

echo "**$RELEASE_DATA**"
echo curl --fail --show-error --request POST --header "PRIVATE-TOKEN:****" --header 'Content-Type:application/json' --data "$RELEASE_DATA" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/releases"

printf "\n\n######### Creating GitLab release from Git tag %s #########\n" "$VERSION"
curl --silent --fail --show-error --request POST --header "PRIVATE-TOKEN:$GITLAB_API_TOKEN" --header 'Content-Type:application/json' --data "$RELEASE_DATA" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/releases"
