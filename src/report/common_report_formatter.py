from collections import OrderedDict
from html_content import HTMLContent
from six.moves.urllib.parse import urlparse


class CommonReportFormatter:

    def __init__(self, wasc):
        self.wasc = wasc

    def format(self, zap_report, spider_scan='', spider_scan_results=''):
        formatted = OrderedDict()

        if zap_report is None or len(zap_report) == 0:
            raise RuntimeError('Unable to create Common Format Report as there is no ZAP report')

        formatted['version'] = '2.5'
        formatted['vulnerabilities'] = self.__format_vulnerabilities(zap_report.get('site', []))

        return formatted

    def __format_vulnerabilities(self, sites):
        alerts = [alert for site in sites for alert in site['alerts']]
        alerts_instances = [(alert, instance) for alert in alerts for instance in alert['instances']]
        sorted_alerts = sorted(alerts_instances, key=lambda ai: (
            -int(ai[0].get('riskcode', '1')),
            ai[0].get('name', ''),
            ai[1].get('method', ''),
            ai[1].get('uri', '')))
        return [self.__format_vulnerability(alert, instance) for (alert, instance) in sorted_alerts]

    def __format_vulnerability(self, alert, instance):
        formatted = OrderedDict()
        formatted['category'] = 'dast'
        formatted['confidence'] = self.__format_confidence(alert.get('confidence', ''))
        formatted['cve'] = alert.get('pluginid', '')
        formatted['description'] = HTMLContent(alert.get('desc', '')).text()
        formatted['identifiers'] = (self.__format_plugin_identifier(alert.get('name', ''), alert.get('pluginid', '')) +
                                    self.__format_cwe_identifier(alert.get('cweid', '')) +
                                    self.__format_wasc_identifier(alert.get('wascid', '')))
        formatted['links'] = self.__format_links(alert.get('reference', ''))
        formatted['location'] = self.__format_location(instance.get('uri', ''),
                                                       instance.get('method', ''),
                                                       instance.get('param', ''))
        formatted['message'] = alert.get('name', '')
        formatted['scanner'] = {'id': 'zaproxy', 'name': 'ZAProxy'}
        formatted['severity'] = self.__format_severity(alert.get('riskcode', ''))
        formatted['solution'] = HTMLContent(alert.get('solution', '')).text()

        if not formatted['cve']:
            raise RuntimeError("Unable to create DAST report as "
                               "there is no pluginid for alert '{}'".format(formatted['message']))

        return formatted

    def __format_severity(self, zap_risk):
        return {'0': 'info', '1': 'low', '2': 'medium', '3': 'high'}.get(zap_risk, 'unknown')

    def __format_confidence(self, zap_confidence):
        return {'0': 'ignore', '1': 'low', '2': 'medium', '3': 'high', '4': 'confirmed'}.get(zap_confidence, 'unknown')

    def __format_links(self, reference):
        urls = HTMLContent(reference).list_of_text_in_tag('p')
        return [{'url': url} for url in urls]

    def __format_location(self, uri, method, param):
        url_parts = urlparse(uri)
        path = url_parts.path

        if url_parts.query:
            path += '?{}'.format(url_parts.query)

        if url_parts.fragment:
            path += '#{}'.format(url_parts.fragment)

        formatted = OrderedDict()
        formatted['hostname'] = '{}://{}'.format(url_parts.scheme, url_parts.netloc)
        formatted['method'] = method
        formatted['param'] = param
        formatted['path'] = path
        return formatted

    def __format_plugin_identifier(self, vulnerability_name, plugin_id):
        formatted = OrderedDict()
        formatted['name'] = vulnerability_name
        formatted['type'] = 'ZAProxy_PluginId'
        formatted['url'] = 'https://github.com/zaproxy/zaproxy/blob/w2019-01-14/docs/scanners.md'
        formatted['value'] = plugin_id
        return [formatted]

    def __format_cwe_identifier(self, cwe_id):
        if not cwe_id:
            return []

        formatted = OrderedDict()
        formatted['name'] = 'CWE-{}'.format(cwe_id)
        formatted['type'] = 'CWE'
        formatted['url'] = 'https://cwe.mitre.org/data/definitions/{}.html'.format(cwe_id)
        formatted['value'] = cwe_id
        return [formatted]

    def __format_wasc_identifier(self, wasc_id):
        if not wasc_id or not wasc_id.isdigit():
            return []

        formatted = OrderedDict()
        formatted['name'] = 'WASC-{}'.format(wasc_id)
        formatted['type'] = 'WASC'
        formatted['url'] = self.wasc.reference_url_for_id(wasc_id)
        formatted['value'] = wasc_id
        return [formatted]
