

class TargetWebsiteAvailabilityCheck:

    def __init__(self, is_available, config, response=None, unavailable_reason=None):
        self._is_available = is_available
        self._config = config
        self._response = response
        self._unavailable_reason = unavailable_reason

    def is_available(self):
        return self._is_available

    def unavailable_reason(self):
        return self._unavailable_reason

    def is_safe_to_scan(self):
        if not self._config.full_scan:
            return True, ''

        if not self._response:
            return False, 'Attempting to full scan, but the site is unavailable. Permissions cannot be verified.'

        permission = self._response.headers.get('gitlab-dast-permission')

        if permission != 'allow' and self._config.full_scan_domain_validation_required:
            return False, 'The application has not explicitly allowed DAST scans'

        if permission == 'deny':
            return False, 'The application has explicitly denied DAST scans'

        return True, ''
