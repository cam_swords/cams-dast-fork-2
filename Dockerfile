ARG ZAP_VERSION=w2019-09-24
FROM owasp/zap2docker-weekly:$ZAP_VERSION

ARG FIREFOX_VERSION=59.0.2
ARG GECKODRIVER_VERSION=0.19.1

USER root

WORKDIR /output

# Add custom ZAP hook that handles authentication
COPY src/hooks.py /home/zap/.zap_hooks.py

# Use a custom log file for ZAP
COPY config/zap-log4j.properties /root/.ZAP_D/log4j.properties

# Wrap zap-baseline.py and zap-full-scan.py to support cli parameters for authentication
RUN mv /zap/zap-baseline.py /zap/zap_baseline_original.py && \
    mv /zap/zap-full-scan.py /zap/zap_full_scan_original.py

COPY --chown=zap src/ /zap/

ADD analyze /analyze

ENTRYPOINT []
CMD ["/analyze"]
