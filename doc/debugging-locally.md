# Debugging DAST and ZAP locally

## Overview

DAST is an application that makes use of [ZAP](https://github.com/zaproxy/zaproxy) to analyze a target site for vulnerabilities.
The DAST analyze script orchestrates a scan by communicating with ZAP using a HTTP REST API. Both the analyze script and ZAP run inside a Docker container, as shown below:

```mermaid
graph LR
subgraph Docker container
Analyze["/analyze"]-->|HTTP REST| Z(ZAP)
end
Z-->|Scan using HTTP| T(Target Site)
```

By following the instructions below, you can temporarily point DAST to a local instance of ZAP running outside the Docker container.
The local instance of ZAP will have a GUI, which means you can watch DAST scans live, and then configure and rerun them afterwards.

```mermaid
graph LR
subgraph Docker container
Analyze["/analyze"]
Z(ZAP, unused)
end

subgraph Local machine
Analyze-->|HTTP REST| ZL(Target Site)
ZL(ZAP)
end
ZL-->|Scan using HTTP| T(Target Site)
```

## Instructions

1. Install ZAP as found on the [OWASP Zed Attack Proxy Site](https://www.owasp.org/index.php/OWASP_Zed_Attack_Proxy_Project).
1. Work out what your IP address is. For a Mac/Linux, run `ifconfig` in a terminal. It's likely the `inet` address of interface `en0`.
1. In ZAP, go to `Preferences`, `Local Proxy`. For address, select your IP address. Ensure the port is `8080`.
1. In ZAP, go to `Preferences`, `API`. It should be `Enabled`, `Disable the API Key`, `Do not require an API key for safe operations`, `Report errors`. Add your IP address to those that are permitted.
1. In a terminal, verify that the API works by running `curl http://[YOUR IP ADDRESS]:8080`. You should see text html from ZAP.
1. In a terminal, run the DAST Docker container, using bash as the command. This will open a bash shell inside the container. e.g. `docker run -ti --rm -v $PWD:/zap/wrk registry.gitlab.com/gitlab-org/security-products/dast:12-3-stable /bin/bash`
1. Run the following command, substituting your IP address.
   ```bash
   sed -i "s/zap_ip + ':' + str(port)/'[YOUR IP ADDRESS]:8080'/g" /zap/zap_*_original.py &&
   sed -i "s/wait_for_zap/#wait_for_zap/" /zap/zap_*_original.py &&
   sed -i "s/zap.core.shutdown/#zap.core.shutdown/" /zap/zap_*_original.py
   ```
1. You can now run the scan from inside the container by running: `/analyze -t [TARGET_WEBSITE]`. Your local ZAP instance should start running the scan you have triggered.

