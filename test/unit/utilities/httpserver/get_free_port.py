import random
import socket


def get_free_port():
    for _ in range(0, 10):
        # https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml
        port = random.randint(49152, 65535)
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        if not sock.connect_ex(('127.0.0.1', port)) == 0:
            sock.close()
            return port
    else:
        raise RuntimeError("Failed to find free port after 10 attempts")
