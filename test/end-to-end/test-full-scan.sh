#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

BUILT_IMAGE=${BUILT_IMAGE:-dast}

setup_suite() {
  mkdir -p output

  # install jq if not present
  command -v jq >/dev/null || apk add jq

  docker network create test >/dev/null

  # start webgoat
  tar -xzvf fixtures/webgoat-data.tar.gz -C ./ >/dev/null 2>&1

  docker run --rm \
    -v "${PWD}/.webgoat-8.0.0.M21":/home/webgoat/.webgoat-8.0.0.M21 \
    --name goat \
    --network test \
    -d \
    registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256:bc09fe2e0721dfaeee79364115aeedf2174cce0947b9ae5fe7c33312ee019a4e >/dev/null

  # start nginx container to use as proxy for domain validation
  docker run --rm \
    -v "${PWD}/fixtures/domain-validation/nginx.conf":/etc/nginx/conf.d/default.conf \
    --name vulnerabletestserver \
    --network test \
    -d \
    nginx:alpine >/dev/null
  true
}

teardown_suite() {
  docker rm -f goat vulnerabletestserver >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  rm -r .webgoat-8.0.0.M21 >/dev/null
  true
}

test_webgoat_full_scan() {
  # /logout is excluded because it invalidates the session, causing many pages to not be spidered
  # css files are excluded to make the test run faster
  # bootstrap is removed because it causes an intermittent vulnerability result
  docker run --rm \
    --env DAST_FULL_SCAN_ENABLED=true \
    --env DAST_FULL_SCAN_DOMAIN_VALIDATION_REQUIRED=true \
    -v "${PWD}":/output \
    --network test \
    "${BUILT_IMAGE}" /analyze -j -t http://vulnerabletestserver/WebGoat/attack \
    --auth-url http://vulnerabletestserver/WebGoat/login \
    --auth-username "someone" \
    --auth-password "P@ssw0rd" \
    --auth-username-field "exampleInputEmail1" \
    --auth-password-field "exampleInputPassword1" \
    --auth-exclude-urls 'http://vulnerabletestserver/WebGoat/js/respond.min.js,http://vulnerabletestserver/.*.css,http://vulnerabletestserver/WebGoat/plugins/bootstrap/css/bootstrap.min.css' \
    >output/test_webgoat_full_scan.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < gl-dast-report.json > output/report_test_webgoat_full_scan.json

  # this cookie sometimes is populated, and sometimes is not. when it is populated, the value will be different each time.
  # note sed -i is not used as it does not work consistently on different operating systems
  sed 's/JSESSIONID=.*<\/p><p><\/p>", "pluginid": "90033"/JSESSIONID=__REMOVED__<\/p><p><\/p>", "pluginid": "90033"/g' gl-dast-report.json > temp.json && mv temp.json gl-dast-report.json

  diff -u <(jq -S 'del(.["@generated"])' expect/test_webgoat_full_scan.json) \
          <(jq -S 'del(.["@generated"])' gl-dast-report.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"
}

test_webgoat_full_scan_domain_validation() {
  docker run --rm\
    --env DAST_FULL_SCAN_ENABLED=True \
    --env DAST_FULL_SCAN_DOMAIN_VALIDATION_REQUIRED=1 \
    -v "${PWD}":/output \
    --network test \
    "${BUILT_IMAGE}" /analyze -t http://goat:8080/WebGoat/attack \
    >output/test_webgoat_full_scan_domain_validation.log 2>&1

  assert_equals "1" "$?" "Expected DAST domain validation to fail but it did not"
}
